
import * as THREE from './three.js/build/three.module.js';
import { OrbitControls } from './three.js/examples/jsm/controls/OrbitControls.js';
import { GCodeLoader } from './three.js/examples/jsm/loaders/GCodeLoader.js';
var container;
var camera, scene, renderer;
init();
animate();
function init() {
  container = document.getElementById('3dsection2' );
  //document.body.appendChild( container );

  camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 10000 );
  camera.position.set( 0, 0, 70 );
  scene = new THREE.Scene();
  var loader = new GCodeLoader();
  loader.load( 'three.js/examples/models/gcode/benchy.gcode', function ( object ) {
    object.position.set( - 100, - 20, 100 );
    scene.add( object );
  } );
  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  renderer.setSize( 300, 300 );
  container.appendChild( renderer.domElement );
  


  var controls = new OrbitControls( camera, renderer.domElement );
  window.addEventListener( 'resize', resize, false );
}
function resize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
  renderer.setSize( window.innerWidth, window.innerHeight );
}
function animate() {
  renderer.render( scene, camera );
  requestAnimationFrame( animate );
}